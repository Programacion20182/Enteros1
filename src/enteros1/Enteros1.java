package enteros1;

// Llamamos a la clase Scanner que se encuentra en el paquete java.util
import java.util.Scanner;

/**
 * Clase de Enteros.
 * @author diego
 */
public class Enteros1 {
    
    /**
     * "Metodo" que va a sumar los primero n numeros enteros positivos.
     * @param n Los n numeros que se van a sumar.
     * @return La suma de los primeros n enteros.
     */
    public static int sumaNEnteros(int n){
        // Variable entera donde se guardara el resultado de la suma.
        int r = 0;
        // Recorremos desde 1 hasta n.
        for(int i = 1; i <= n; i++)
            // Se actualiza el valor de r.
            r = r + i;
        // Regresamos el valor de r.
        return r;
    }

    /**
     * Metodo donde ocurre la magia.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Suma de los primeros:
        int n = 17;
        
        // Creamos el objeto Scanner.
        Scanner valores = new Scanner(System.in);
        
        System.out.println("¿Cual es el valor de n? ");
        
        // El valor entero que le pasemos en consola se guardara en la variable n.
        n = valores.nextInt();
        
        // Variable entera donde se guardara el resultado.
        // Se inicia en 0 para que no afecte al resultado final.
        /*
        int r = 0;
        */
        
        // for contiene un bloque de instrucciones que se repite hasta que
        // la condicion deja de cumplirse.
        // int i = 0; -> Variable de inicio
        // i <= 10;  -> La condicion
        // i++ -> i = i + 1
        
        /*
        for(int i = 1; i <= 10; i++){
            r = r + i;
        } // Fin del for
        
        */

        // Instruccion para imprimir en pantalla.
        // El simbolo + en este caso sirve para pegar 2 cadenas.
        // Llamamos al metodo sumaNEnteros que se encarga de sumar de 1 hasta n.
        System.out.println("La suma de los primeros " + n + " enteros es: " + sumaNEnteros(n));
        
        // Le damos un nuevo valor a r.
        // No se vuelve a declarar porque ya hay una variable con el identificador
        // r dentro del mismo alcance de main.
        /*
        r = 1;
        */
        
        // La variable i del for anterior muere al terminar el for anterior, 
        // por lo que en necesario volver a declararla de nuevo.
        // Si al for no se le ponen llaves, entonces sola la linea que sigue 
        // se tomara.
        /*
        for(int i = 1; i <= 10; i++)
            r = r * i;
        
        System.out.println("La multiplicacion de los primeros " + 10 + " enteros es: " + r);
        */
        
    } // Fin del main 
    
} // Fin de la clase
